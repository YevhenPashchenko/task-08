package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Task;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public void parse(List<Task> container) {
		XMLInputFactory factory = XMLInputFactory.newFactory();
		try(FileInputStream fis = new FileInputStream(xmlFileName)) {
			XMLEventReader reader = factory.createXMLEventReader(fis);
			Task task = null;
			int currentOperand = 0;
			while (reader.hasNext()) {
				XMLEvent event = reader.nextEvent();
				if (event.isStartElement()) {
					StartElement startElement = event.asStartElement();
					switch (startElement.getName().getLocalPart()) {
						case "tasks":
							String schemaName = null;
							Iterator<Attribute> iterator = startElement.getAttributes();
							while (iterator.hasNext()) {
								Attribute attribute = iterator.next();
								if ("schemaLocation".equals(attribute.getName().getLocalPart())) {
									schemaName = attribute.getValue().split(" ")[1];
								}
							}
							if (schemaName != null) {
								SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
								Schema schema = sf.newSchema(new File(schemaName));
								Validator validator = schema.newValidator();
								validator.validate(new StreamSource(xmlFileName));
							}
							break;
						case "task":
							task = new Task();
							break;
						case "name":
							task.setName(reader.nextEvent().asCharacters().getData());
							break;
						case "operand":
							int[] operand = task.getOperand();
							operand[currentOperand] = Integer.parseInt(reader.nextEvent().asCharacters().getData());
							currentOperand++;
							task.setOperand(operand);
							break;
						case "result":
							task.setResult(Integer.parseInt(reader.nextEvent().asCharacters().getData()));
							break;
					}

				}
				if (event.isEndElement()) {
					if ("task".equals(event.asEndElement().getName().getLocalPart())) {
						container.add(task);
						currentOperand = 0;
					}
				}
			}
		} catch (XMLStreamException | SAXException | IOException e) {
			e.printStackTrace();
		}

	}

	public void writeToFile(String xmlFileName, List<Task> container) {
		try(ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
			XMLOutputFactory factory = XMLOutputFactory.newInstance();
			XMLStreamWriter writer = factory.createXMLStreamWriter(outputStream);

			writer.writeStartDocument("utf-8", "1.0");
			writer.writeStartElement("ts", "tasks", "http://com.my:8080/doc");
			writer.writeNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
			writer.writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "schemaLocation", "http://com.my:8080/doc input.xsd");
			writer.writeNamespace("ts", "http://com.my:8080/doc");

			for (Task task:
				 container) {
				writer.writeStartElement("task");

				writer.writeStartElement("name");
				writer.writeCharacters(task.getName());
				writer.writeEndElement();

				for (int i = 0; i < task.getOperand().length; i++) {
					writer.writeStartElement("operand");
					writer.writeCharacters(String.valueOf(task.getOperand()[i]));
					writer.writeEndElement();
				}

				writer.writeStartElement("result");
				writer.writeCharacters(String.valueOf(task.getResult()));
				writer.writeEndElement();

				writer.writeEndElement();
			}

			writer.writeEndElement();
			writer.flush();
			writer.close();

			String xml = outputStream.toString(StandardCharsets.UTF_8);

			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

			transformer.transform(new StreamSource(new StringReader(xml)), new StreamResult(xmlFileName));
		} catch (TransformerException | XMLStreamException | IOException e) {
			e.printStackTrace();
		}
	}
}