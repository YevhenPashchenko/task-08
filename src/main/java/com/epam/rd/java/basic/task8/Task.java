package com.epam.rd.java.basic.task8;

public class Task {

    private String name;
    private int[] operand = new int[2];
    private int result;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int[] getOperand() {
        return operand;
    }

    public void setOperand(int[] operand) {
        this.operand = operand;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }
}
