package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Task;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public void parse(List<Task> container) {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser saxParser = factory.newSAXParser();
			DefaultHandler handler = new DefaultHandler() {
				final String NAME = "name";
				final String OPERAND = "operand";
				final String RESULT = "result";
				String currentElement = "";
				int currentIndexOperand = 0;

				Task task;

				@Override
				public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
					switch (qName) {
						case "ts:tasks":
							String schemaName = attributes.getValue("xsi:schemaLocation").split(" ")[1];
							SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
							Schema schema = sf.newSchema(new File(schemaName));
							Validator validator = schema.newValidator();
							try {
								validator.validate(new StreamSource(new File(xmlFileName)));
							} catch (IOException e) {
								e.printStackTrace();
							}
							break;
						case "task":
							task = new Task();
							break;
						case NAME:
							currentElement = NAME;
							break;
						case OPERAND:
							currentElement = OPERAND;
							break;
						case RESULT:
							currentElement = RESULT;
					}
				}

				@Override
				public void endElement(String uri, String localName, String qName) {
					switch (qName) {
						case "task":
							container.add(task);
							break;
						case NAME:
						case RESULT:
							currentElement = "";
							break;
						case OPERAND:
							currentElement = "";
							currentIndexOperand++;
					}
				}

				@Override
				public void characters(char[] ch, int start, int length) {
					switch (currentElement) {
						case NAME:
							task.setName(String.valueOf(ch, start, length));
							break;
						case OPERAND:
							if (currentIndexOperand > 1) {
								currentIndexOperand = 0;
							}
							int[] operand = task.getOperand();
							operand[currentIndexOperand] = Integer.parseInt(String.valueOf(ch, start, length));
							task.setOperand(operand);
							break;
						case RESULT:
							task.setResult(Integer.parseInt(String.valueOf(ch, start, length)));
					}
				}
			};
			saxParser.parse(xmlFileName, handler);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
	}

	public void writeToFile(String xmlFileName, List<Task> container) {
		try(FileWriter fw = new FileWriter(xmlFileName)) {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			DefaultHandler handler = new DefaultHandler() {

				final String NAME = "name";
				final String OPERAND = "operand";
				final String RESULT = "result";
				final String END = "end";
				String currentElement = "";
				int currentIndexOperand = 0;
				int currentPositionInWrittenFile = 0;

				Task task;

				@Override
				public void startElement(String uri, String localName, String qName, Attributes attributes) {
					switch (qName) {
						case "task":
							if (task == null) {
								task = container.get(0);
							} else {
								task = container.get(container.indexOf(task) + 1);
							}
							break;
						case NAME:
							currentElement = NAME;
							break;
						case OPERAND:
							currentElement = OPERAND;
							break;
						case RESULT:
							currentElement = RESULT;
					}
				}

				@Override
				public void endElement(String uri, String localName, String qName) {
					switch (qName) {
						case NAME:
							currentElement = "";
							break;
						case RESULT:
							if (container.indexOf(task) + 1 == container.size()) {
								currentElement = END;
							} else {
								currentElement = "";
							}
							break;
						case OPERAND:
							currentElement = "";
							currentIndexOperand++;
							break;
					}
				}

				@Override
				public void characters(char[] ch, int start, int length) {
					switch (currentElement) {
						case NAME:
							try {
								fw.write(ch, currentPositionInWrittenFile, start - currentPositionInWrittenFile);
								fw.write(task.getName());
								currentPositionInWrittenFile = start + length;
							} catch (IOException e) {
								e.printStackTrace();
							}
							break;
						case OPERAND:
							if (currentIndexOperand > 1) {
								currentIndexOperand = 0;
							}
							try {
								fw.write(ch, currentPositionInWrittenFile, start - currentPositionInWrittenFile);
								fw.write(String.valueOf(task.getOperand()[currentIndexOperand]));
								currentPositionInWrittenFile = start + length;
							} catch (IOException e) {
								e.printStackTrace();
							}
							break;
						case RESULT:
							try {
								fw.write(ch, currentPositionInWrittenFile, start - currentPositionInWrittenFile);
								fw.write(String.valueOf(task.getResult()));
								currentPositionInWrittenFile = start + length;
							} catch (IOException e) {
								e.printStackTrace();
							}
							break;
						case END:
							try {
								fw.write(ch, currentPositionInWrittenFile, String.valueOf(ch).lastIndexOf("tasks>") + "tasks>".length() - currentPositionInWrittenFile);
								currentElement = "";
							} catch (IOException e) {
								e.printStackTrace();
							}
							break;
					}
				}
			};
			saxParser.parse(this.xmlFileName, handler);
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
	}
}