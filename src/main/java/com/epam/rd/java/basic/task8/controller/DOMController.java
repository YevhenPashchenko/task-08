package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Task;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public void parse(List<Task> container) {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
			Document document = db.parse(new FileInputStream(xmlFileName));

			String schemaName = document.getDocumentElement().getAttribute("xsi:schemaLocation").split(" ")[1];
			SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = sf.newSchema(new File(schemaName));
			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(new File(xmlFileName)));

			NodeList nodeList = document.getDocumentElement().getElementsByTagName("task");
			for (int i = 0; i < nodeList.getLength(); i++) {
				Task task = new Task();
				String name;
				int[] operands = new int[2];
				int result;
				Element element = (Element) nodeList.item(i);
				name = element.getElementsByTagName("name").item(0).getTextContent();
				for (int j = 0; j < operands.length; j++) {
					operands[j] = Integer.parseInt(element.getElementsByTagName("operand").item(j).getTextContent());
				}
				result = Integer.parseInt(element.getElementsByTagName("result").item(0).getTextContent());
				task.setName(name);
				task.setOperand(operands);
				task.setResult(result);
				container.add(task);
			}
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
	}

	public void writeToFile(String xmlFileName, List<Task> container) {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document document = db.newDocument();
			Element rootElement = document.createElementNS("http://com.my:8080/doc", "xs:tasks");
			rootElement.setAttributeNS("http://www.w3.org/2001/XMLSchema-instance",
					"xsi:schemaLocation",
					"http://com.my:8080/doc input.xsd");
			for (Task task:
				 container) {
				Element elementTask = document.createElement("task");
				Element elementName = document.createElement("name");
				elementName.appendChild(document.createTextNode(task.getName()));
				elementTask.appendChild(elementName);
				for (int i = 0; i < task.getOperand().length; i++) {
					Element elementOperand = document.createElement("operand");
					elementOperand.appendChild(document.createTextNode(String.valueOf(task.getOperand()[i])));
					elementTask.appendChild(elementOperand);
				}
				Element elementResult = document.createElement("result");
				elementResult.appendChild(document.createTextNode(String.valueOf(task.getResult())));
				elementTask.appendChild(elementResult);
				rootElement.appendChild(elementTask);
			}

			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

			transformer.transform(new DOMSource(rootElement), new StreamResult(xmlFileName));
		} catch (ParserConfigurationException | TransformerException e) {
			e.printStackTrace();
		}
	}

}
