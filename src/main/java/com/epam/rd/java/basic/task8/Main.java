package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import java.util.ArrayList;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		List<Task> tasks = new ArrayList<>();

		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		domController.parse(tasks);

		// sort (case 1)
		// PLACE YOUR CODE HERE
		SortContainer.sortByTaskName(tasks);

		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		domController.writeToFile(outputXmlFile, tasks);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		tasks = new ArrayList<>();

		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		saxController.parse(tasks);

		// sort  (case 2)
		// PLACE YOUR CODE HERE
		SortContainer.sortByMaxOperand(tasks);

		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		saxController.writeToFile(outputXmlFile, tasks);
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		tasks = new ArrayList<>();

		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		staxController.parse(tasks);

		// sort  (case 3)
		// PLACE YOUR CODE HERE
		SortContainer.sortByResult(tasks);

		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		staxController.writeToFile(outputXmlFile, tasks);
	}

}
