package com.epam.rd.java.basic.task8;

import java.util.Comparator;
import java.util.List;

public class SortContainer {

    public static void sortByTaskName(List<Task> container) {
        container.sort(Comparator.comparing(Task::getName));
    }

    public static void sortByMaxOperand(List<Task> container) {
        container.sort((t1, t2) -> t2.getOperand()[0] - t1.getOperand()[0]);
    }

    public static void sortByResult(List<Task> container) {
        container.sort(Comparator.comparingInt(Task::getResult));
    }

}
